Counting words from STDIN and output result to STDOUT.


**USAGE:**
You can use stdin redirection or pipe:
```python word_counter/counter.py -N 5 < /var/log/syslog```
```cat /var/log/syslog | python word_counter/counter.py -N 5```