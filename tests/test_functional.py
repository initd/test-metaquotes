import os
import subprocess


def test_counting():
    parent_dir = os.path.join(os.path.dirname(__file__), "..")
    pipe = subprocess.PIPE
    script_path = os.path.join(parent_dir, "word_counter", "counter.py")
    counter_p = subprocess.Popen(
        ("python", script_path, "-N", "5"),
        stdin=pipe, stdout=pipe)
    input_file_path = os.path.join(parent_dir, "input.txt")
    with open(input_file_path) as input_file:
        for line in input_file:
            counter_p.stdin.write(line)
    counter_p.stdin.close()
    result = counter_p.stdout.read()
    with open(os.path.join(parent_dir, "output.txt")) as output_file:
        expected = output_file.read()
    assert len(expected.split("\n")) == len(result.split("\n"))
    for line in result:
        assert line in expected


if __name__ == '__main__':
    # Due not to use pytest as requirements but expect using it in future, we
    # are using tests written in pytest manner with custom launcher.
    test_counting()
