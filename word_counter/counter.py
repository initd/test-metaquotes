import argparse
import multiprocessing
import multiprocessing.managers
import Queue
import signal
import sys


def mgr_init():
    """Initializer for SyncManager to ignore KeyboardInterrupt."""
    signal.signal(signal.SIGINT, signal.SIG_IGN)


class TasksDispatcher(object):
    """Tasks dispatcher class. Used as main target."""
    def __init__(self, queue, word_dict, exit_event):
        self.queue = queue
        self.words = word_dict
        self.exit_event = exit_event

    def split_words(self, line):
        """Split words from line. Put them into queue to count."""
        for word in line.split():
            self.queue.put(("count_word", word))

    def count_word(self, word):
        """Counts words."""
        if word not in self.words:
            self.words[word] = 0
        self.words[word] += 1

    def run(self):
        """Main worker function. Exits on exit event."""
        while True:
            try:
                current_task, arg = self.queue.get(True, 1)
                getattr(self, current_task)(arg)
            except Queue.Empty:
                if self.exit_event.is_set():
                    break


def get_dict_manager():
    """Returns instance of shared dictionary."""
    sync_manager = multiprocessing.managers.SyncManager()
    sync_manager.start(mgr_init())
    return sync_manager.dict()


def main(process_count):
    def finalize():
        """Finalize work properly."""
        exit_event.set()
        queue.close()
        for process in processes:
            process.join()
        for word, count in words.items():
            sys.stdout.writelines("{0} {1}\n".format(word, count))
    try:
        queue = multiprocessing.Queue()
        words = get_dict_manager()
        exit_event = multiprocessing.Event()
        processes = []
        for _ in xrange(process_count):
            task_dispatcher = TasksDispatcher(queue, words, exit_event)
            process = multiprocessing.Process(target=task_dispatcher.run)
            processes.append(process)

        for process in processes:
            process.start()

        for data in sys.stdin:
            queue.put(("split_words", data))

        finalize()

    except KeyboardInterrupt:
        print "Interrupted"
        finalize()
        exit(127)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="Multiprocessing word counter")
    parser.add_argument(
        "-N", "--processes",
        type=int,
        default=multiprocessing.cpu_count(),
        help="Count of processes to start"
    )
    args = parser.parse_args()
    main(args.processes)
